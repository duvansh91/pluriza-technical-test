import './App.css';
import logo from './logo-pluriza.png'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} alt="logo" />
        <p>
          Technical Assessment
        </p>
      </header>
    </div>
  );
}

export default App;
