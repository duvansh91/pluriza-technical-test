# Pluriza technical assessment _Part 1_

This React App was configured to be deployed on 3 different **S3** buckets.

## To run this project:
```sh
npm install
```
```sh
npm start
```
## To deploy the app:
Make some changes and `push` a commit into `master`.

All the apps are hosted in **S3**, also are configured with **CloudFront** and **Route53** to be accessible by a custom domain.

## To access the websites go to:
[plurizatest.tk](https://plurizatest.tk)\
[plurizatest2.tk](https://plurizatest2.tk)\
[plurizatest3.tk](https://plurizatest3.tk)
